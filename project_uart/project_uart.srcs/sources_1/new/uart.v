`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Engineer: Kim Geonu
//
// Create Date: 2016/05/23 15:10:53
// Module Name: uart
//////////////////////////////////////////////////////////////////////////////////


module uart(
    input            rst,
    input            clk,
    input      [1:0] CtrlParity,
    input            TxDataEn,
    input      [7:0] TxData,
    output reg       RxDataRdy,
    output reg [7:0] RxData,
    output reg       Error,
    input            Rx,
    output reg       Tx  );

reg       RxLock;
reg       TxLock;
reg       TxParity;
reg [7:0] TxDataCopy;

always @(rst) begin
    if (!rst) begin
        RxDataRdy <= 1'b0;
        RxData    <= 8'b0;
        Error     <= 1'b0;
        Tx        <= 1'b1;
        RxLock    <= 1'b0;
        TxLock    <= 1'b0;
    end
end

always @(negedge Rx) begin
    if (!RxLock) begin
        RxLock <= 1'b1;
        Error  <= 1'b0;
        @(posedge clk);
        repeat(8) begin
            @(posedge clk);
            RxData <= {Rx, RxData[7:1]};
        end
        if (CtrlParity[1]) begin
            @(posedge clk);
            if ((^RxData) ^ Rx ^ CtrlParity[0])
                Error <= 1'b1;
        end
        RxDataRdy <= 1'b1;
        @(posedge clk);
        if (!Rx)
            Error <= 1'b1;
        RxDataRdy <= 1'b0;
        RxLock    <= 1'b0;
    end
end

always @(negedge TxDataEn) begin
    if (!TxLock) begin
        TxLock     <= 1'b1;
        Tx         <= 1'b0;
        TxParity   <= ^TxData;
        TxDataCopy <= TxData;
        @(posedge clk);
        repeat(8) begin
            Tx <= {TxDataCopy[0]};
            TxDataCopy <= {1'b0, TxDataCopy[7:1]};
            @(posedge clk);
        end
        if (CtrlParity[1]) begin
            Tx <= TxParity ^ CtrlParity[0];
            @(posedge clk);
        end
        Tx     <= 1'b1;
        TxLock <= 1'b0;
    end
end
endmodule
