`timescale 1 ps / 1 ps

module uart_ggb (
    rst,
    clk,
    CtrlParity,
    TxDataEn,
    TxData,
    RxDataRdy,
    RxData,
    Error,
    Rx,
    Tx);
    input rst;
    input clk;
    input [1:0]CtrlParity;
    input TxDataEn;
    input [7:0]TxData;
    output RxDataRdy;
    output [7:0]RxData;
    output Error;
    input Rx;
    output Tx;
    reg Tx;
    reg [7:0]RxData;
    reg Error;
    reg RxDataRdy;
    reg [3:0]TxCount;
    reg Parity;
    reg RParity;
    reg [3:0]RxCount;
    reg GoRx;
    reg GoTx;
    reg EnRdy;
    //transmitter
    always @(rst)begin
        if(~rst)begin
            Tx <= 1;
            Error <= 0;
            RxDataRdy <= 0;
            RxCount <= 0;
            RxData <= 0;
            GoRx <= 0;
            TxCount <= 0;
            GoTx <= 0;
            EnRdy <= 0;
        end
    end
    always @(posedge TxDataEn) begin
        EnRdy <= 1;
    end
    always @(negedge TxDataEn) begin
        if(EnRdy == 1) begin
            Parity = CtrlParity[0];
            Tx <= 0;
            TxCount <= 0;
            GoTx <= 1;
        end  
    end
    always @(posedge clk or negedge rst) begin
        if(TxCount >= 0 && TxCount <= 7 && GoTx == 1)begin
            Tx <= TxData[TxCount];
            Parity <= Parity ^ TxData[TxCount];
            TxCount <= TxCount + 1;
        end
    end 
    always @(posedge clk or negedge rst) begin
        if(TxCount > 7 && GoTx == 1) begin
            if(CtrlParity[1] && TxCount == 8) begin
                Tx <= Parity;
                TxCount <= TxCount + 1;
            end
            else begin
                Tx <= 1'b1;
                GoTx <= 0;
            end
        end
    end 
    //receiver
    always @(negedge Rx) begin
        if(GoRx == 0) begin
            GoRx <= 1;
            RxCount <= 0;
            RParity <= CtrlParity[0];
        end
    end
    always @(posedge clk) begin
        if(GoRx == 1 && RxCount == 0) begin
            RxCount <= RxCount + 1;
        end
        if(GoRx == 1 && RxCount >= 1 && RxCount <= 8) begin
            RxData <= {Rx, RxData[7:1]};
            RParity <= RParity ^ Rx;
            RxCount <= RxCount + 1;
        end
    end
    always @(posedge clk) begin
        if(GoRx == 1 && RxCount == 8 + CtrlParity[1]) begin
            RxDataRdy <= 1;
            GoRx <= 0;
            if(CtrlParity[1] == 1) begin
                if(Rx != RParity) begin
                    Error <= 1;
                end
            end
            @(posedge clk) begin
                RxDataRdy <= 0;
                if(Rx != 1) begin
                    Error <= 0;
                end
            end
        end
    end

endmodule