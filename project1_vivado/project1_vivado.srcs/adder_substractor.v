module addsub
(
    A, B, M,
    S, C, V, N, Z
);

input   [7:0] A;
input   [7:0] B;
input         M;
output  [7:0] S;
output        C;
output        V;
output        N;
output        Z;

wire    [7:0] Ci;
wire    [7:0] Mbyte;

assign Mbyte = {M, M, M, M, M, M, M, M};

assign C = Ci[7];
assign V = Ci[7] ^ Ci[6];
assign N = S[7];
assign Z = ~(S[0] | S[1] | S[2] | S[3] | S[4] | S[5] | S[6] | S[7]);

full_adder U0 [7:0] (A, B ^ Mbyte, {Ci[6:0],M}, S, Ci);

endmodule