module comparator
(
    A, B,
    EQ, GT, LT, GE, LE
);

input   [7:0] A;
input   [7:0] B;
output        EQ;
output        GT;
output        LT;
output        GE;
output        LE;

wire    [7:0] S;
wire          C;
wire          V;
wire          N;
wire          Z;

wire          RN;
assign RN = N ^ V;

assign EQ = Z;
assign GT = ~(RN | Z);
assign LT = RN;
assign GE = ~RN;
assign LE = RN | Z;

addsub U1_BA
(
    .A  (A      ),
    .B  (B      ),
    .M  (1'b1   ),
    .S  (S      ),
    .C  (C      ),
    .V  (V      ),
    .N  (N      ),
    .Z  (Z      )
);

endmodule


