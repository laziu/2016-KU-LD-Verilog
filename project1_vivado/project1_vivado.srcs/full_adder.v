module full_adder
(
  A,
  B,
  Ci,
  S,
  Co
);

input   A;
input   B;
input   Ci;
output  S;
output  Co;

//Implement your logic below.

//not U0 (S , A);
//not U0 (Co, A);

assign S = A^B^Ci;
assign Co = (A&B)|(B&Ci)|(Ci&A);

endmodule