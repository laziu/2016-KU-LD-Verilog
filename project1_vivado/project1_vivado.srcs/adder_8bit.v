module adder_8bit
(
  A,
  B,
  S,
  Co
);

input   [7:0] A;
input   [7:0] B;
output  [7:0] S;
output        Co;

//Implement your logic below.

//not U0 (S[0], A[0]);
//not U0 (S[1], A[1]);
//not U0 (S[2], A[2]);
//not U0 (S[3], A[3]);
//not U0 (S[4], A[4]);
//not U0 (S[5], A[5]);
//not U0 (S[6], A[6]);
//not U0 (S[7], A[7]);
//not U0 (Co  , A[0]);
//not U0 (V   , A[0]);

wire    [7:0] C;
assign Co = C[7];
full_adder U0 [7:0] (A,B,{C[6:0],1'b0},S,C);

endmodule