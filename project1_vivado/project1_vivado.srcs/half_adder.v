module half_adder
(
  A,
  B,
  S,
  Co
);

input   A;
input   B;
output  S;
output  Co;

//Implement your logic below.

//not U0 (S ,A);
//not U0 (Co,A);

assign S  = A^B;
assign Co = A&B;

endmodule