module calculator (
  input             rst,
  input             clk,
  output reg [ 7:0] addr_insn,
  input      [15:0] data_insn,
  output reg        finished,
  output reg        undefined,
  output     [31:0] reg0,
  output     [31:0] reg1,
  output     [31:0] reg2,
  output     [31:0] reg3,
  output     [31:0] reg4,
  output     [31:0] reg5,
  output     [31:0] reg6,
  output     [31:0] reg7,
  output     [31:0] reg8,
  output     [31:0] reg9,
  output     [31:0] rega,
  output     [31:0] regb,
  output     [31:0] regc,
  output     [31:0] regd,
  output     [31:0] rege,
  output     [31:0] regf  );
  
  parameter OP_NOP = 4'd0;
  parameter OP_FIN = 4'd1;
  parameter OP_ADD = 4'd2;
  parameter OP_SUB = 4'd3;
  parameter OP_MUL = 4'd4;
  parameter OP_AND = 4'd5;
  parameter OP_OR  = 4'd6;
  parameter OP_XOR = 4'd7;
  parameter OP_SLL = 4'd8;
  parameter OP_SRL = 4'd9;
  parameter OP_SRA = 4'd10;
  parameter OP_LDI = 4'd11;
  
  reg [31:0] cache [0:15];
  
  assign reg0 = cache[4'h0];
  assign reg1 = cache[4'h1];
  assign reg2 = cache[4'h2];
  assign reg3 = cache[4'h3];
  assign reg4 = cache[4'h4];
  assign reg5 = cache[4'h5];
  assign reg6 = cache[4'h6];
  assign reg7 = cache[4'h7];
  assign reg8 = cache[4'h8];
  assign reg9 = cache[4'h9];
  assign rega = cache[4'ha];
  assign regb = cache[4'hb];
  assign regc = cache[4'hc];
  assign regd = cache[4'hd];
  assign rege = cache[4'he];
  assign regf = cache[4'hf];

  always @(rst) begin
    if (!rst) begin
      addr_insn <=  8'b0;
      finished  <=  1'b0;
      undefined <=  1'b0;
	  cache[4'h0] <= 32'b0;
      cache[4'h1] <= 32'b0;
      cache[4'h2] <= 32'b0;
      cache[4'h3] <= 32'b0;
      cache[4'h4] <= 32'b0;
      cache[4'h5] <= 32'b0;
      cache[4'h6] <= 32'b0;
      cache[4'h7] <= 32'b0;
      cache[4'h8] <= 32'b0;
      cache[4'h9] <= 32'b0;
      cache[4'ha] <= 32'b0;
      cache[4'hb] <= 32'b0;
      cache[4'hc] <= 32'b0;
      cache[4'hd] <= 32'b0;
      cache[4'he] <= 32'b0;
      cache[4'hf] <= 32'b0;
	end
  end
  
  always @(posedge clk) begin
    if (rst) begin
        case (data_insn[15:12])
          OP_ADD: cache[data_insn[11:8]] <= cache[data_insn[7:4]] + cache[data_insn[3:0]];      
          OP_SUB: cache[data_insn[11:8]] <= cache[data_insn[7:4]] - cache[data_insn[3:0]];
          OP_MUL: cache[data_insn[11:8]] <= $signed(cache[data_insn[7:4]]) * $signed(cache[data_insn[3:0]]);
          OP_AND: cache[data_insn[11:8]] <= cache[data_insn[7:4]] & cache[data_insn[3:0]];
          OP_OR : cache[data_insn[11:8]] <= cache[data_insn[7:4]] | cache[data_insn[3:0]];
          OP_XOR: cache[data_insn[11:8]] <= cache[data_insn[7:4]] ^ cache[data_insn[3:0]];
          OP_SLL: cache[data_insn[11:8]] <= cache[data_insn[7:4]] << cache[data_insn[3:0]];
          OP_SRL: cache[data_insn[11:8]] <= cache[data_insn[7:4]] >> cache[data_insn[3:0]];
          OP_SRA: cache[data_insn[11:8]] <= $signed(cache[data_insn[7:4]]) >>> cache[data_insn[3:0]];          
          OP_LDI: cache[data_insn[11:8]] <= {{25{data_insn[7]}}, data_insn[6:0]};
        endcase
        undefined <= OP_LDI < data_insn[15:12];
        if (~finished)
          addr_insn <= addr_insn + 8'd1;
	end
  end
  
  always @(data_insn[15:12]) begin
    if (data_insn[15:12] == OP_FIN)
      finished <= 1'b1;
  end
endmodule