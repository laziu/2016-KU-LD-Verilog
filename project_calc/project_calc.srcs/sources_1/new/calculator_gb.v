`timescale 1ps / 1ps

module calculator_gb(
    rst,
    clk,
    addr_insn,
    data_insn,
    finished,
    undefined,
    reg0,
    reg1,
    reg2,
    reg3,
    reg4,
    reg5,
    reg6,
    reg7,
    reg8,
    reg9,
    rega,
    regb,
    regc,
    regd,
    rege,
    regf
    );
     input rst;
     input clk;
     output [7:0]addr_insn;
     input [15:0]data_insn;
     output finished;
     output undefined;
     output [31:0]reg0;
     output [31:0]reg1;
     output [31:0]reg2;
     output [31:0]reg3;
     output [31:0]reg4;
     output [31:0]reg5;
     output [31:0]reg6;
     output [31:0]reg7;
     output [31:0]reg8;
     output [31:0]reg9;
     output [31:0]rega;
     output [31:0]regb;
     output [31:0]regc;
     output [31:0]regd;
     output [31:0]rege;
     output [31:0]regf;
     reg [31:0]register [0:15];
     reg finished;
     reg undefined;
     reg [7:0]addr_insn;
     assign reg0 = register[0];
     assign reg1 = register[1];
     assign reg2 = register[2];
     assign reg3 = register[3];
     assign reg4 = register[4];
     assign reg5 = register[5];
     assign reg6 = register[6];
     assign reg7 = register[7];
     assign reg8 = register[8];
     assign reg9 = register[9];
     assign rega = register[10];
     assign regb = register[11];
     assign regc = register[12];
     assign regd = register[13];
     assign rege = register[14];
     assign regf = register[15];
     parameter NOP = 4'd0;
     parameter ADD = 4'd2;
     parameter SUB = 4'd3;
     parameter MUL = 4'd4;
     parameter AND = 4'd5;
     parameter OR = 4'd6;
     parameter XOR = 4'd7;
     parameter SLL = 4'd8;
     parameter SRL = 4'd9;
     parameter SRA = 4'd10;
     parameter LDI = 4'd11;
     always @(posedge clk or negedge rst) begin
        if(~rst) begin
             register[0] <= 0;
             register[1] <= 0;
             register[2] <= 0;
             register[3] <= 0;
             register[4] <= 0;
             register[5] <= 0;
             register[6] <= 0;
             register[7] <= 0;
             register[8] <= 0;
             register[9] <= 0;
             register[10] <= 0;
             register[11] <= 0;
             register[12] <= 0;
             register[13] <= 0;
             register[14] <= 0;
             register[15] <= 0;
             finished <= 0;
             undefined <= 0;
             addr_insn <= 0;
        end
        else begin
            case(data_insn[15:12])
                NOP: ;
                ADD: begin
                    register[data_insn[11:8]] <= register[data_insn[7:4]] + register[data_insn[3:0]];
                end
                SUB: begin
                    register[data_insn[11:8]] <= register[data_insn[7:4]] - register[data_insn[3:0]];
                end
                MUL: begin
                    register[data_insn[11:8]] <= register[data_insn[7:4]] * register[data_insn[3:0]];
                end
                AND: begin
                    register[data_insn[11:8]] <= register[data_insn[7:4]] & register[data_insn[3:0]];
                end
                OR: begin
                    register[data_insn[11:8]] <= register[data_insn[7:4]] | register[data_insn[3:0]];
                end
                XOR: begin
                    register[data_insn[11:8]] <= register[data_insn[7:4]] ^ register[data_insn[3:0]];
                end
                SLL: begin
                    register[data_insn[11:8]] <= register[data_insn[7:4]] << register[data_insn[3:0]];
                end
                SRL: begin
                    register[data_insn[11:8]] <= register[data_insn[7:4]] >> register[data_insn[3:0]];
                end
                SRA: begin
                    if(data_insn[7]) begin
                        register[data_insn[11:8]] <= {31{data_insn[7]}} & (register[data_insn[7:4]] >> register[data_insn[3:0]]);
                    end
                    else begin
                        register[data_insn[11:8]] <= register[data_insn[7:4]] >> register[data_insn[3:0]];
                    end
                end
                LDI: begin
                    register[data_insn[11:8]] <= {{25{data_insn[7]}}, data_insn[6:0]};
                end
                default: begin
                    undefined <= 1;
                end
            endcase
        end
     end

     always @(posedge clk) begin
        addr_insn <= addr_insn + 1;
     end

     always @(data_insn) begin
        if (data_insn[15:12] == 4'b0001)
            finished <= 1;
     end
endmodule
