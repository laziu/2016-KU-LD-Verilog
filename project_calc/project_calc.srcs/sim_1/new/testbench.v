`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 2016/05/21 17:38:35
// Design Name: 
// Module Name: testbench
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module testbench;

parameter MEM_SIZE = 256;

reg             clk;
reg             rst;
reg     [15:0]  mem_insn [0:MEM_SIZE-1];
reg     [ 7:0]  insn [0:1];
wire    [ 7:0]  addr_insn;
wire    [15:0]  data_insn;

wire            finished;
wire            undefined;
wire    [31:0]  reg0;
wire    [31:0]  reg1;
wire    [31:0]  reg2;
wire    [31:0]  reg3;
wire    [31:0]  reg4;
wire    [31:0]  reg5;
wire    [31:0]  reg6;
wire    [31:0]  reg7;
wire    [31:0]  reg8;
wire    [31:0]  reg9;
wire    [31:0]  rega;
wire    [31:0]  regb;
wire    [31:0]  regc;
wire    [31:0]  regd;
wire    [31:0]  rege;
wire    [31:0]  regf;

integer fd;
integer i;

initial begin
  clk <= 1'b0;
  forever #5 clk <= ~clk;
end

initial begin
  rst <= 1'b0;
  #100;
  rst <= 1'b1;
end

initial begin
  fd = $fopen("program.bin","rb");
  if(fd == 0) begin
    $display("Fail to open file");
    $finish;
  end
  i=0;
  while(!$feof(fd)) begin
    $fread(insn,fd);
    mem_insn[i] = {insn[1],insn[0]};
    i=i+1;
  end
end

initial begin
  #(10*MEM_SIZE+100)
  $display("Time over");
  $display("Simulation is finished.");
  $finish;
end

always @(posedge clk) begin
  if(finished) begin
    $display("finished signal is set from calculator.");
    $display("Simulation is finished.");
    $finish;
  end
end

assign data_insn = mem_insn[addr_insn[7:0]];

calculator U_CALC(
  .rst        (rst        ),
  .clk        (clk        ),
  .addr_insn  (addr_insn  ),
  .data_insn  (data_insn  ),
  .finished   (finished   ),
  .undefined  (undefined  ),
  .reg0       (reg0       ),
  .reg1       (reg1       ),
  .reg2       (reg2       ),
  .reg3       (reg3       ),
  .reg4       (reg4       ),
  .reg5       (reg5       ),
  .reg6       (reg6       ),
  .reg7       (reg7       ),
  .reg8       (reg8       ),
  .reg9       (reg9       ),
  .rega       (rega       ),
  .regb       (regb       ),
  .regc       (regc       ),
  .regd       (regd       ),
  .rege       (rege       ),
  .regf       (regf       )
);

endmodule
